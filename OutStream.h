#pragma once
#include <stdio.h>
class OutStream
{
protected:
	FILE * _file;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
	void endline();
};


class FileStream: public OutStream
{
public:
	FileStream(char* str);
	~FileStream();
};

class OutStreamEncrypted : public OutStream
{
private:
	int heist;
public:
	OutStreamEncrypted(int num);
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	//OutStreamEncrypted& operator<<(void(*pf)());
};